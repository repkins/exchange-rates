# Exchange Rates

## Sistēmas prasības

- Frontend
    - **npm** ^5
    - **node** ^6
- Backend
    - **php** >= 7.1.3
    - PHP paplašinājumi:
        - **OpenSSL**
        - **PDO**
        - **Mbstring**
        - **Tokenizer**
        - **XML**
        - **Ctype**
        - **JSON**
    - **composer** >= 1.6.3

## Uzstādīšana

1. Noklonot git repozitoriju 

        git clone https://repkins@bitbucket.org/repkins/exchange-rates.git

2. Pāriet uz noklonoto repozitoriju direktorijā `exchange-rates`
3. Apakšdirektorijā `server` izpildīt komandu `composer install`
4. Apakšdirektorijā `client` izpildīt komandu `npm install`
5. Kad 3. punktā minēta komanda ir izpildījusies:

    1. Pāriet uz direktoriju `server`
    2. Nokopēt failu `.env.example` un pārsaukt uz `.env`
    3. Izpildīt sekojošas komandas:

            php artisan key:generate
            php artisan jwt:secret
        
    4. Atvert failu `.env` ar teksta redaktoru un veikt tajā konfigurācijas izmaiņas, kas attiecas uz:
        - **MySQL** datubāzes servera savienojumu (sākas ar `DB_`)
        - **Google** servisa API savienojumu (sākas ar `GL_`)
        - **Facebook** servisa API savienojumu (sākas ar `FB_`)
    5. Kad 4. punkta minētajā failā ir pabeigtas *MySQL savienojuma konfigurācijas* izmaiņas, tad izpildīt komandu `php artisan migrate`
    6. Palaist izstrādes web serveri, izmantojot komandu `php artisan serve`
    7. Pārliecināties, vai ir savienojums ar web serveri, pārlūkā pārejot uz http://localhost:8000. Tai jāatbild ar noklusēto Laravel skatu.


6. Kad 4. punktā minēta komanda ir izpildījusies:
    1. Pāriet uz direktoriju `client`
    2. Palaist "frontend" izstrādes serveri, izmantojot komandu `npm start -- --ssl`

        > Piezīme. Servera palaišanas laikā tiks uzbūvēti pašparakstīti sertifikāti priekš SSL savienojuma no pārlūka

    3. Pārliecināties, lai ir savienojums ar "frontend" serveri un ir veiksmīgi palaidusies "frontend" aplikācija, pārlūkā pārejot uz https://localhost:4200. 

        Pēc aplikācijas palaišanas jābūt redzamam ielogošanas skatam ar uzrakstu lielajos fontos "Welcome to Exchange Rates app" un zem tā 2 sociālās ielogošanas pogām.

        > **N.B!** Pārlūks var neļaut izveidot savienojumu ar "frontend" aplikāciju, tā vietā paziņojot par nedrošu savienojumu. Parasti to var apiet, ja pārlūks to ļauj, parasti izmantojot mazāk izcelto saiti paziņojuma skatā.
          
### Valūtu kursu datu ieguve

Datus par valūtu kursiem ir iespējams ielādēt un saglabāt datubāzē, apakšdirektorijā `server` izpildot komandu 

    php artisan command:fetch-rates

Priekš cronjob ieraksta izmantot komandu 

    php <ceļš-uz-artisan-failu-server-apakšdirektorijā> command:fetch-rates

kurā frāzi "`<ceļš-uz-artisan-failu-server-apakšdirektorijā>`" aizvietot ar tajā teikto.

### Autentifikācijas mēģinājumu žurnalēšana

Žurnāls ar autentifikācijas mēģinājumiem glabājas teksta failos, kuri sākas ar `auth-`, apakšdirektorijā `server/storage/logs`. Katrs šāds fails atbilst tās izveidošanas dienas iežurnalizētiem ierakstiem.
