import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { environment } from 'environments/environment';

import { SessionService } from './session.service';
import { AuthResp } from './resp.model';

import { oauthUri, oauthCallbackUri, authMeUri } from './auth.constants';

@Injectable()
export class AuthService {

  private oauthUrl = `${environment.apiHostname}/${oauthUri}`;
  private callbackUrl = `${window.location.protocol}//${window.location.host}/oauth/callback/:provider`;


  get accessToken() {
    const { accessToken } = this.sess.getSessionData();
    return accessToken;
  }

  constructor(
    private http: HttpClient,
    private sess: SessionService
  ) { }

  getProviderRedirectUrl(provider: SocialProvider) {
    const httpParams = new HttpParams().set('callback_url', this.makeCallbackUrl(provider));

    return this.oauthUrl.replace(':provider', provider) + `?${httpParams.toString()}`;
  }

  async continueAuth(provider: SocialProvider, queryParams: any) {
    const params = new HttpParams({ fromObject: queryParams })
      .set('callback_url', this.makeCallbackUrl(provider));

    const providedCallbackUri = oauthCallbackUri.replace(':provider', provider);

    try {
      const { access_token } = await this.http
        .get<AuthResp>(`${environment.apiHostname}/${providedCallbackUri}`, { params })
        .toPromise();

      this.sess.openSession(access_token);
    } catch (err) {
      this.handleError(err);
    }
  }

  async checkLogin() {
    const accessToken = this.accessToken;

    if (accessToken) {
      const headers = new HttpHeaders({
        'Authorization': 'Bearer ' + accessToken
      });

      try {
        const loginData = await this.http
          .get(`${environment.apiHostname}/${authMeUri}`, { headers })
          .toPromise();

        return loginData;
      } catch (err) {

        if (err instanceof HttpErrorResponse && err.status === 401) {
          return null;
        } else {
          this.handleError(err);
        }
      }
    } else {
      return null;
    }
  }

  private makeCallbackUrl(provider: SocialProvider) {
    return this.callbackUrl.replace(':provider', provider);
  }

  private handleError(error: any) {
    console.error(error);

    throw error;
  }
}

export type SocialProvider = 'google' | 'facebook';
