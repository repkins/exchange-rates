import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) {}

  async canActivate() {
    const loginInfo = await this.auth.checkLogin();

    if (loginInfo) {
      return true;
    } else {
      this.router.navigate([ '/login' ]);

      return false;
    }
  }
}
