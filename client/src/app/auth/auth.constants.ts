export const oauthUri = 'api/oauth/:provider';
export const oauthCallbackUri = 'api/oauth/:provider/callback';

export const authMeUri = 'api/user';
