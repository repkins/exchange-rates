import { Provider } from '@angular/core';

import { AuthService } from './auth.service';
import { SessionService } from './session.service';
import { AuthGuard } from './guard.service';

export const authProviders: Provider[] = [
  AuthService,
  SessionService,
  AuthGuard,
];
