import { Injectable } from '@angular/core';
import { SessionStorageService } from 'app/storage/session-storage.service';

@Injectable()
export class SessionService {

  constructor(private sessionStorage: SessionStorageService) { }

  openSession(accessToken: string) {
    const sessData: SessionAuth = { accessToken };

    this.sessionStorage.set('auth', sessData);
  }

  closeSession() {
    this.sessionStorage.remove('auth');
  }

  getSessionData() {
    return this.sessionStorage.get<SessionAuth>('auth');
  }
}

interface SessionAuth {
  accessToken: string;
}
