import { NgModule } from '@angular/core';
import { authProviders } from './auth.providers';

@NgModule({
  providers: [ ...authProviders ]
})
export class AuthModule { }
