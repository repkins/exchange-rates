import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { StorageModule } from './storage/storage.module';
import { AppRoutingModule } from './app-routing.module';

import { AuthModule } from './auth/auth.module';
import { LoginModule } from './login/login.module';
import { RatesModule } from './rates/rates.module';

import { AppComponent } from './app.component';
import { appCode } from './app.constants';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    StorageModule.withPrefix(appCode),
    AppRoutingModule,

    AuthModule,
    LoginModule,
    RatesModule,
  ],
  providers: [ Title ],
  bootstrap: [ AppComponent ],
})
export class AppModule { }
