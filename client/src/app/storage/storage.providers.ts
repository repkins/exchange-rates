import { Provider } from '@angular/core';

import { SessionStorageService } from './session-storage.service';

export const storageProviders: Provider[] = [
  SessionStorageService,
];
