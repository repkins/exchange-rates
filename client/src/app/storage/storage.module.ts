import { NgModule, ModuleWithProviders } from '@angular/core';

import { storageProviders } from './storage.providers';
import { STORAGE_PREFIX } from './storage.tokens';

@NgModule({
  providers: [ ...storageProviders ]
})
export class StorageModule {

  static withPrefix(prefix: string): ModuleWithProviders {
    return {
      ngModule: StorageModule,
      providers: [
        { provide: STORAGE_PREFIX, useValue: prefix }
      ]
    };
  }
}
