import { Injectable, Inject } from '@angular/core';
import { STORAGE_PREFIX } from './storage.tokens';

@Injectable()
export class SessionStorageService {

  constructor(@Inject(STORAGE_PREFIX) private prefix: string) { }

  set(key: string, value: any) {
    const fullKey = this.getFullKey(key);

    window.sessionStorage.setItem(fullKey, JSON.stringify(value));
  }

  remove(key: string) {
    const fullKey = this.getFullKey(key);

    window.sessionStorage.removeItem(fullKey);
  }

  get<T>(key: string): T {
    const fullKey = this.getFullKey(key);

    return JSON.parse(window.sessionStorage.getItem(fullKey));
  }

  private getFullKey(key: string) {
    return `${this.prefix}.${key}`;
  }
}
