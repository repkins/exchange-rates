export interface RespUser {
  name: string;
  avatar: string;
}

export interface RespRatesSnapshot {
  published_at: string;
  rates: RespRate[];
}
export interface RespRate {
  currency: string;
  rate: string;
}
