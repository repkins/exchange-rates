import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'environments/environment';

import { AuthService } from 'app/auth/auth.service';
import { RespUser, RespRatesSnapshot } from './resp.models';

import { profileUri, ratesUri } from './rates.constants';

@Injectable()
export class RatesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  async fetchUser() {
    const accessToken = this.auth.accessToken;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + accessToken
    });

    try {
      const respUser = await this.http
        .get<RespUser>(`${environment.apiHostname}/${profileUri}`, { headers })
        .toPromise();

      return respUser;
    } catch (err) {
      this.handleError(err);
    }
  }

  async fetchRates() {
    const accessToken = this.auth.accessToken;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + accessToken
    });

    try {
      const respRates = await this.http
        .get<RespRatesSnapshot[]>(`${environment.apiHostname}/${ratesUri}`, { headers })
        .toPromise();

      return respRates;
    } catch (err) {
      this.handleError(err);
    }
  }


  private handleError(error: any) {
    console.error(error);

    throw error;
  }
}
