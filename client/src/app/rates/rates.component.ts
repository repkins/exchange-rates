import { Component, OnInit } from '@angular/core';

import { RatesService } from './rates.service';
import { RespUser, RespRatesSnapshot } from './resp.models';

@Component({
  templateUrl: './rates.component.html'
})
export class RatesComponent implements OnInit {

  user: RespUser;
  rates: RespRatesSnapshot[];

  constructor(private ratesSvc: RatesService) { }

  async ngOnInit() {
    this.user = await this.ratesSvc.fetchUser();
    this.rates = await this.ratesSvc.fetchRates();
  }

}
