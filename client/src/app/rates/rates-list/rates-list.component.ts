import { Component, Input } from '@angular/core';
import { RespRatesSnapshot } from '../resp.models';

@Component({
  selector: 'app-rates-list',
  templateUrl: './rates-list.component.html'
})
export class RatesListComponent {

  @Input() rates: RespRatesSnapshot[];

}
