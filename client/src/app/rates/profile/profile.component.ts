import { Component, Input } from '@angular/core';
import { RespUser } from '../resp.models';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent {

  @Input() user: RespUser;

}
