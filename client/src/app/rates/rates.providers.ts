import { Provider } from '@angular/core';
import { RatesService } from './rates.service';

export const ratesProviders: Provider[] = [
  RatesService,
];
