import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'app/auth/guard.service';

import { RatesComponent } from './rates.component';

const routes: Routes = [
  { path: 'rates', component: RatesComponent, canActivate: [ AuthGuard ] },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RatesRoutingModule { }
