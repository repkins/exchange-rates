import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { RatesRoutingModule } from './rates-routing.module';

import { RatesComponent } from './rates.component';
import { ProfileComponent } from './profile/profile.component';
import { RatesListComponent } from './rates-list/rates-list.component';

import { ratesProviders } from './rates.providers';

@NgModule({
  declarations: [
    RatesComponent,
    ProfileComponent,
    RatesListComponent,
  ],
  imports: [
    CommonModule,
    TabsModule.forRoot(),

    RatesRoutingModule,
  ],
  providers: [ ...ratesProviders ]
})
export class RatesModule { }
