import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { appName } from './app.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(private title: Title ) {
    this.title.setTitle(appName);
  }
}
