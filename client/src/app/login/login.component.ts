import { Component } from '@angular/core';
import { AuthService, SocialProvider } from 'app/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  constructor(private auth: AuthService) { }

  getSocialRedirectUrl(provider: SocialProvider) {
    return this.auth.getProviderRedirectUrl(provider);
  }
}
