import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login.component';
import { ProviderCallbackComponent } from './provider/provider-callback.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'oauth/callback/:provider', component: ProviderCallbackComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LoginRoutingModule { }
