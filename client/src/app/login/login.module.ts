import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'ngx-bootstrap/alert';

import { LoginRoutingModule } from './login-routing.module';

import { LoginComponent } from './login.component';
import { ProviderCallbackComponent } from './provider/provider-callback.component';

@NgModule({
  declarations: [
    LoginComponent,
    ProviderCallbackComponent,
  ],
  imports: [
    CommonModule,
    AlertModule.forRoot(),

    LoginRoutingModule,
  ]
})
export class LoginModule { }
