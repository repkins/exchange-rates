import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService, SocialProvider } from 'app/auth/auth.service';

@Component({
  templateUrl: './provider-callback.component.html'
})
export class ProviderCallbackComponent implements OnInit {

  hasErr = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() {
    const provider = this.route.snapshot.paramMap.get('provider') as SocialProvider;
    const queryParams = this.route.snapshot.queryParams;

    this.callbackProvider(provider, queryParams);
  }

  private async callbackProvider(provider: SocialProvider, queryParams: any) {
    try {
      await this.auth.continueAuth(provider, queryParams);

      this.router.navigate(['rates']);
    } catch (err) {
      this.hasErr = true;
    }
  }
}
