<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('oauth/{driver}', 'Api\Auth\SocialAuthController@redirectToProvider');
Route::get('oauth/{driver}/callback', 'Api\Auth\SocialAuthController@handleProviderCallback');

Route::middleware('auth:api')->get('user', 'Api\UserController@me');
Route::middleware('auth:api')->get('rates', 'Api\RatesController@index');
