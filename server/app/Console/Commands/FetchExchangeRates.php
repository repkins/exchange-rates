<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\ExchangeSnapshot;

use Feeds;

class FetchExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:fetch-rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches exchange rates';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        $feed = Feeds::make('https://www.bank.lv/vk/ecb_rss.xml');

        foreach ($feed->get_items() as $item) {
            $published_at = Carbon::createFromTimestamp($item->get_date('U'));
            
            $existing_snapshot = ExchangeSnapshot::where('published_at', $published_at)->first();
            if ($existing_snapshot) {
                continue;
            }
            
            $snapshot = new ExchangeSnapshot;
            $snapshot->published_at = $published_at;
            $snapshot->save();
            
            $rates = preg_split('/\s(?=[A-Z])/', $item->get_description());
            foreach ($rates as $rate_raw) {
                $rate = explode(' ', $rate_raw);
                $currency = $rate[0];
                $value = $rate[1];
                
                $snapshot->rates()->create([
                    'currency' => $currency,
                    'rate' => $value
                ]);
            }
        }
        
        $this->info('Exchange Rates has been fetched successfully');
    }
}
