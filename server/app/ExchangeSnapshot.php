<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeSnapshot extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'published_at'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at'];
    

    /**
     * Get the rates for the snapshot.
     */
    public function rates()
    {
        return $this->hasMany('App\ExchangeRate', 'snapshot_id');
    }
}
