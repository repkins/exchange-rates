<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ExchangeSnapshot;

class RatesController extends Controller {
    
    /**
     * Returns exchange rates grouped by daily snapshots
     *
     * @return array
     */
    public function index()
    {
        $snapshots = ExchangeSnapshot::with('rates')->orderBy('published_at', 'desc')->get();

        return $snapshots->toArray();
    }
}
