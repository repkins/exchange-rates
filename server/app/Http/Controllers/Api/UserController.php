<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    /**
     * Return currently authenticated user
     *
     * @return mixed
     */
    public function me(Request $request)
    {
        return $request->user();
    }
}
