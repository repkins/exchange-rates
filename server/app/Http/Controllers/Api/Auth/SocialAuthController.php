<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;

use Socialite;
use Auth;
use Log;
use Exception;

class SocialAuthController extends Controller
{

    /**
     * @var Illuminate\Log\Logger
     */
    protected $authLogger;
    
    public function __construct()
    {
        $this->middleware('guest');
        
        $this->authLogger = Log::channel('auth');
    }

    /**
     * List of providers configured in config/services acts as whitelist
     *
     * @var array
     */
    protected $providers = [
        'google',
        'facebook'
    ];

    /**
     * Redirect to provider for authentication
     *
     * @param $request
     * @param $driver
     * @return mixed
     */
    public function redirectToProvider(Request $request, $driver)
    {
        if( ! $this->isProviderAllowed($driver) ) {
            $this->authLogger->error("Tried to authenticate with unsupported social provider {$driver}");
        
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        $callback_url = $request->query('callback_url');
        if ($callback_url) {
            Socialite::driver($driver)->redirectUrl($callback_url);
        }
        
        try {
            return Socialite::driver($driver)->stateless()->redirect();
        } catch (Exception $e) {
            $this->authLogger->error("Error on redirection to social provider {$driver}", [ 'msg' => $e->getMessage() ]);
            
            return $this->sendFailedResponse($driver, $e->getMessage());
        }
    }

    /**
     * Handle response of authentication redirect callback
     *
     * @param $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleProviderCallback(Request $request, $driver)
    {
        $callback_url = $request->query('callback_url');
        if ($callback_url) {
            Socialite::driver($driver)->redirectUrl($callback_url);
        }
        
        try {
            $user = Socialite::driver($driver)->stateless()->user();
        } catch (Exception $e) {
            $this->authLogger->error("Error after redirection from social provider {$driver}", [ 'msg' => $e->getMessage() ]);
            
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        if (empty($user->email)) {
            $this->authLogger->error("No email id returned from {$driver} provider");
            
            return $this->sendFailedResponse("No email id returned from {$driver} provider.");
        } else {
            return $this->loginOrCreateAccount($user, $driver);
        }
    }

    /**
     * Send a successful response
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendSuccessResponse($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Send a failed response with a msg
     *
     * @param null $msg
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFailedResponse($msg = null)
    {
        return response()
            ->json(['msg' => $msg ?: 'Unable to login, try with another provider to login.'], 400);
    }

    protected function loginOrCreateAccount($providerUser, $driver)
    {
        // check for already has account
        $user = User::where('email', $providerUser->getEmail())->first();

        // if user already found
        if( $user ) {
            // update the avatar and provider that might have changed
            $user->update([
                'avatar' => $providerUser->avatar,
                'provider' => $driver,
                'provider_id' => $providerUser->id,
                'access_token' => $providerUser->token
            ]);
        } else {
            // create a new user
            $user = User::create([
                'name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'avatar' => $providerUser->getAvatar(),
                'provider' => $driver,
                'provider_id' => $providerUser->getId(),
                'access_token' => $providerUser->token,
                // user can use reset password to create a password
                'password' => ''
            ]);
        }

        // login the user
        $token = Auth::login($user);
        
        $this->authLogger->info("User {$user->email} is logged in using {$driver}");

        return $this->sendSuccessResponse($token);
    }

    /**
     * Check for provider allowed and services configured
     *
     * @param $driver
     * @return bool
     */
    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }
}
